if [ $# -ne 2 ]
then 
    echo "usage: aires2root.sh <input> <output>" 
    exit;
fi

infile=$1
outfile=$2
CMD=/vol/app/users/harm/software/aires2root/airesParticlesToTXT
$CMD $1 part_temp.txt
rm __*
CMD2=/vol/app/users/harm/software/aires2root/toRoot
$CMD2 part_temp.txt $2
rm "part_temp.txt"
