#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TTree.h"

using namespace std;

int main(int argc, char** argv) {

  if (argc != 3) {
    cout << "usage: ./toRoot <infile> <outfile>" << endl;  
  }
  
  TFile f(argv[2],"recreate");
  TTree tree("tree","particles");
  double    logr, weight,ux,uy,theta,t,logE;
  int pid;  
  tree.Branch("logE",&logE);
  tree.Branch("logr",&logr);
  tree.Branch("theta",&theta);
  tree.Branch("ux",&ux);
  tree.Branch("uy",&uy);
  tree.Branch("t",&t);
  tree.Branch("weight",&weight);
  tree.Branch("pid",&pid);

  ifstream ifs(argv[1]);
  string dum;
  getline(ifs,dum);
  //pid,logE,logr,theta,ux,uy,t,weight
  while ( ifs >> pid >> logE >> logr>> theta>> ux>> uy>> t >> weight )
    tree.Fill();
  
  tree.Write();
  f.Close();
  
  return 0;
  
}
