# aires2root

Tested with AIRES version  19040800

mix `c` and `c++` code to convert ground particle file to a txt and or root file. 

The shell scripts wrap around the executables, and clean-up the mess that `Aires` generates.

Examples in the `*.sh` files, you might need to modify the path. 
```
usage: aires2root.sh <input> <output>
```
or 
```
usage: aires2txt.sh <input> <output>
```

Packages needed: 
```
ROOT Aires gfortan
```
