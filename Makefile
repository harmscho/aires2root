
#AIRESLIBS = -L/vol/app/software/aires/install/lib -lAires /Users/harmscho/AirShowerSim/2-8-4a/src/aires/fieldparser.o -L /lib/x86_64-linux-gnu/ -lgfortran
AIRESLIBS = -L/vol/app/software/aires/install/lib -lAires -L /lib/x86_64-linux-gnu/ -lgfortran

ROOT_INC = $(shell root-config --cflags)
ROOT_LIB = $(shell root-config --libs)


all: ciodemoc_.c toRoot
	gcc ciodemoc_.c -o airesParticlesToTXT $(AIRESLIBS)
#$(ROOT_INC) $(ROOT_LIB) $(CXXFLAGS)

toRoot: toRoot.cc
	g++ toRoot.cc -o toRoot $(ROOT_INC) $(ROOT_LIB)

long: long_reader.c
	gcc long_reader.c -o airesLongReader $(AIRESLIBS) 
